/* flashlight-window.c
 *
 * Copyright 2020 Arnaud Ferraris
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "flashlight-config.h"
#include "flashlight-window.h"

#define FLASH_SYSFS_PATH "/sys/devices/platform/led-controller/leds/white:flash/brightness"

struct _FlashlightWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  GtkToggleButton     *button;
  GtkImage            *button_icon;
};

G_DEFINE_TYPE (FlashlightWindow, flashlight_window, GTK_TYPE_APPLICATION_WINDOW)

static gboolean enable_flashlight(gboolean enable)
{
  int fd = open(FLASH_SYSFS_PATH, O_WRONLY);

  if (fd < 0) {
    g_critical("Unable to open file %s", FLASH_SYSFS_PATH);
    return FALSE;
  }

  write(fd, enable ? "1" : "0", 1);
  close(fd);

  return TRUE;
}

static void on_button_toggled_cb(GtkToggleButton  *button,
                                 FlashlightWindow *self)
{
  gboolean enable = gtk_toggle_button_get_active(button);
  gchar *icon_name = enable ? "weather-clear-symbolic" : "display-brightness-symbolic";

  g_return_if_fail (FLASHLIGHT_IS_WINDOW (self));

  enable_flashlight(enable);
  gtk_image_set_from_icon_name(self->button_icon, icon_name, GTK_ICON_SIZE_DIALOG);
}

static void flashlight_window_class_init(FlashlightWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);

  gtk_widget_class_set_template_from_resource(widget_class, "/com/a-wai/flashlight/flashlight-window.ui");
  gtk_widget_class_bind_template_child(widget_class, FlashlightWindow, button);
  gtk_widget_class_bind_template_child(widget_class, FlashlightWindow, button_icon);
  gtk_widget_class_bind_template_callback(widget_class, on_button_toggled_cb);
}

static void flashlight_window_init (FlashlightWindow *self)
{
  gtk_widget_init_template(GTK_WIDGET(self));
  if (!g_file_test(FLASH_SYSFS_PATH, G_FILE_TEST_EXISTS))
    gtk_widget_set_sensitive(GTK_WIDGET(self->button), FALSE);
}
